from fastapi import FastAPI
from authenticator import authenticator
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts, edamam, ingredients, preferences

app = FastAPI()


app.include_router(accounts.router, tags=['accounts'])
app.include_router(authenticator.router, tags=['login'])
app.include_router(edamam.router, tags=['edamam'])
app.include_router(ingredients.router, tags=['ingredients'])
app.include_router(preferences.router, tags=['preferences'])


origins = [
    os.environ.get("CORS_HOST", "http://localhost"),
    "http://localhost:3000",
    "https://what-the-fridge.sept-ct-11.mod3projects.com",
    "https://demogorgons11.gitlab.io/whats-in-the-fridge",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# @app.get("/api/launch-details")
# def launch_details():
#     return {
#         "launch_details": {
#             "year": 2022,
#             "month": 12,
#             "day": "9",
#             "hour": 19,
#             "min": 0,
#             "tz:": "PST"
#         }
#     }
