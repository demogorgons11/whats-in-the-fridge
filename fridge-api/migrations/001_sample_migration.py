

steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(200) NOT NULL UNIQUE,
            hashed_password VARCHAR(200) NOT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """
    ],
    [
        """
        CREATE TABLE ingredients (
            id SERIAL PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            quantity TEXT NULL,
            expiration_date DATE NULL,
            account_id_ingredient INTEGER NOT NULL
                REFERENCES accounts(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE fridge;
        """
    ],
    [
        """
        CREATE TABLE diet (
            id SERIAL PRIMARY KEY NOT NULL,
            balanced BOOLEAN NOT NULL DEFAULT false,
            high_fiber BOOLEAN NOT NULL DEFAULT false,
            high_protein BOOLEAN NOT NULL DEFAULT false,
            low_carb BOOLEAN NOT NULL DEFAULT false,
            low_fat BOOLEAN NOT NULL DEFAULT false,
            low_sodium BOOLEAN NOT NULL DEFAULT false,
            account_id_diet INT NOT NULL UNIQUE
                REFERENCES accounts(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE diet;
        """
    ],
    [
        """
        CREATE TABLE health (
            id SERIAL PRIMARY KEY NOT NULL,
            alcohol_cocktail BOOLEAN NOT NULL DEFAULT false,
            alcohol_free BOOLEAN NOT NULL DEFAULT false,
            celery_free BOOLEAN NOT NULL DEFAULT false,
            crustacean_free BOOLEAN NOT NULL DEFAULT false,
            dairy_free BOOLEAN NOT NULL DEFAULT false,
            DASH BOOLEAN NOT NULL DEFAULT false,
            egg_free BOOLEAN NOT NULL DEFAULT false,
            fish_free BOOLEAN NOT NULL DEFAULT false,
            fodmap_free BOOLEAN NOT NULL DEFAULT false,
            gluten_free BOOLEAN NOT NULL DEFAULT false,
            immuno_supportive BOOLEAN NOT NULL DEFAULT false,
            keto_friendly BOOLEAN NOT NULL DEFAULT false,
            kidney_friendly BOOLEAN NOT NULL DEFAULT false,
            kosher BOOLEAN NOT NULL DEFAULT false,
            low_fat_abs BOOLEAN NOT NULL DEFAULT false,
            low_potassium BOOLEAN NOT NULL DEFAULT false,
            low_sugar BOOLEAN NOT NULL DEFAULT false,
            lupine_free BOOLEAN NOT NULL DEFAULT false,
            Mediterranean BOOLEAN NOT NULL DEFAULT false,
            mollusk_free BOOLEAN NOT NULL DEFAULT false,
            mustard_free BOOLEAN NOT NULL DEFAULT false,
            no_oil_added BOOLEAN NOT NULL DEFAULT false,
            paleo BOOLEAN NOT NULL DEFAULT false,
            peanut_free BOOLEAN NOT NULL DEFAULT false,
            pescatarian BOOLEAN NOT NULL DEFAULT false,
            pork_free BOOLEAN NOT NULL DEFAULT false,
            red_meat_free BOOLEAN NOT NULL DEFAULT false,
            sesame_free BOOLEAN NOT NULL DEFAULT false,
            shellfish_free BOOLEAN NOT NULL DEFAULT false,
            soy_free BOOLEAN NOT NULL DEFAULT false,
            sugar_conscious BOOLEAN NOT NULL DEFAULT false,
            sulfite_free BOOLEAN NOT NULL DEFAULT false,
            tree_nut_free BOOLEAN NOT NULL DEFAULT false,
            vegan BOOLEAN NOT NULL DEFAULT false,
            vegetarian BOOLEAN NOT NULL DEFAULT false,
            wheat_free BOOLEAN NOT NULL DEFAULT false,
            account_id_health INT NOT NULL UNIQUE
                REFERENCES accounts(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE health;
        """
    ]
]
