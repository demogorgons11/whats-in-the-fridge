from queries.pool import pool
from pydantic import BaseModel


class Account(BaseModel):
    id: int
    username: str
    hashed_password: str


class AccountOut(BaseModel):
    id: int
    username: str


class AccountIn(BaseModel):
    username: str
    hashed_password: str


class AccountsOut(BaseModel):
    accounts: list[AccountOut]


class AccountsQueries:
    def get(self, username: str) -> Account:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id
                         , username
                         , hashed_password
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username]
                )
                record = result.fetchone()
                if record is None:
                    return None
                return Account(
                    id=record[0],
                    username=record[1],
                    hashed_password=record[2],
                )

    def get_all_accounts(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username
                    FROM accounts
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def create(self, account: AccountIn, hashed_password: str) -> Account:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO accounts (username, hashed_password)
                    VALUES (%s, %s)
                    RETURNING id;
                    """,
                    [account.username, hashed_password]
                )
                id = result.fetchone()[0]
                return Account(
                    id=id,
                    username=account.username,
                    hashed_password=hashed_password,
                )

    def delete_account(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM accounts
                    WHERE id = %s
                    """,
                    [id],
                )

    def get_account(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username
                    FROM accounts
                    WHERE id = %s
                """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record
