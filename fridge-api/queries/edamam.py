from queries.pool import pool
import requests
import os


def get_diet(account_id):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT balanced
                    , high_fiber
                    , high_protein
                    , low_carb
                    , low_fat
                    , low_sodium
                    , account_id_diet
                FROM diet
                WHERE account_id_diet = %s
            """,
                [account_id],
            )

            record = None
            row = cur.fetchone()
            if row is not None:
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

            results = ""
            for key, value in record.items():
                if value is True and key != "account_id_diet":
                    results = results + "&diet=" + key

            return results.replace("_", "-")


def get_health(account_id):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT alcohol_cocktail
                    , alcohol_free
                    , celery_free
                    , crustacean_free
                    , dairy_free
                    , dash
                    , egg_free
                    , fish_free
                    , fodmap_free
                    , gluten_free
                    , immuno_supportive
                    , keto_friendly
                    , kidney_friendly
                    , kosher
                    , low_fat_abs
                    , low_potassium
                    , low_sugar
                    , lupine_free
                    , mediterranean
                    , mollusk_free
                    , mustard_free
                    , no_oil_added
                    , paleo
                    , peanut_free
                    , pescatarian
                    , pork_free
                    , red_meat_free
                    , sesame_free
                    , shellfish_free
                    , soy_free
                    , sugar_conscious
                    , sulfite_free
                    , tree_nut_free
                    , vegan
                    , vegetarian
                    , wheat_free
                    , account_id_health
                FROM health
                WHERE account_id_health = %s
            """,
                [account_id],
            )

            record = None
            row = cur.fetchone()
            if row is not None:
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

            results = ""
            for key, value in record.items():
                if value is True and key != "account_id_health":
                    if key == "dash":
                        results = results + "&health=DASH"
                    elif key == "mediterranean":
                        results = results + "&health=Mediterranean"
                    else:
                        results = results + "&health=" + key

            return results.replace("_", "-")


class EdamamQueries:

    def get_recipe_by_id(self, id: str, requester):
        the_key = os.environ['EDAMAM_KEY']
        the_id = os.environ['EDAMAM_ID']
        res = requests.get(f'https://api.edamam.com/api/recipes/v2/{id}?type=public&app_id={the_id}&app_key={the_key}')
        data = res.json()
        return {
            "label": data["recipe"]["label"],
            "image": data["recipe"]["image"],
            "url": data["recipe"]["url"]
        }

    def get_recipes_by_query(self, query: str, account_id: int):
        the_key = os.environ['EDAMAM_KEY']
        the_id = os.environ['EDAMAM_ID']
        diet = get_diet(account_id)
        health = get_health(account_id)
        res = requests.get(f'https://api.edamam.com/api/recipes/v2?type=public&q={query}&app_id={the_id}&app_key={the_key}{diet}{health}')
        data = res.json()
        lst = []
        for i in data["hits"]:
            recipe = []
            recipe.append(i["recipe"]["label"])
            recipe.append(i["recipe"]["image"])
            recipe.append(i["recipe"]["url"])
            lst.append(recipe)
        return lst
