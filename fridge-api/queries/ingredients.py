from queries.pool import pool
from pydantic import BaseModel
from typing import Optional
from datetime import date


class DuplicateIngredientError(ValueError):
    pass


class Ingredient(BaseModel):
    id: int
    name: str
    quantity: str
    expiration_date: Optional[date]
    account_id_ingredient: int


class IngredientIn(BaseModel):
    name: str
    quantity: str
    expiration_date: Optional[date]


class IngredientOut(BaseModel):
    id: int
    name: str
    quantity: str
    expiration_date: Optional[date]
    account_id_ingredient: int


class IngredientsOut(BaseModel):
    ingredients: list[IngredientOut]


class IngredientsQueries:
    def get_all_ingredients(self, account_id_ingredient: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, quantity,
                        expiration_date, account_id_ingredient
                    FROM ingredients
                    WHERE account_id_ingredient = %s
                    ORDER BY account_id_ingredient
                    """,
                    [account_id_ingredient]
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results

    def get_ingredient(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, quantity,
                        expiration_date, account_id_ingredient
                    FROM ingredients
                    WHERE id = %s
                    """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def create_ingredient(self, ingredient: IngredientIn, account_id_ingredient: int) -> Ingredient:
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO ingredients (name, quantity
                    , expiration_date, account_id_ingredient)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id
                    """,
                    [
                        ingredient.name,
                        ingredient.quantity,
                        ingredient.expiration_date,
                        account_id_ingredient
                    ],
                )
                id = result.fetchone()[0]
                return Ingredient(
                    id=id,
                    name=ingredient.name,
                    quantity=ingredient.quantity,
                    expiration_date=ingredient.expiration_date,
                    account_id_ingredient=account_id_ingredient,
                )

    def update_ingredient(self, account_id_ingredient, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.name,
                    data.quantity,
                    data.expiration_date,
                    account_id_ingredient
                ]
                cur.execute(
                    """
                    UPDATE ingredients
                    SET name = %s
                      , quantity = %s
                      , expiration_date = %s
                    WHERE id = %s
                    RETURNING id, name, quantity, expiration_date, account_id_ingredient
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def delete_ingredient(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM ingredients
                    WHERE id = %s
                    """,
                    [id],
                )
