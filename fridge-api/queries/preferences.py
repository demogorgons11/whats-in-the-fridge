from queries.pool import pool
from pydantic import BaseModel


class Diet(BaseModel):
    id: int
    balanced: bool
    high_fiber: bool
    high_protein: bool
    low_carb: bool
    low_fat: bool
    low_sodium: bool
    account_id_diet: int


class DietIn(BaseModel):
    balanced: bool = False
    high_fiber: bool = False
    high_protein: bool = False
    low_carb: bool = False
    low_fat: bool = False
    low_sodium: bool = False

    class Config:
        schema_extra = {
            'example': {
                'balanced': 'false',
                'high_fiber': 'false',
                'high_protein': 'false',
                'low_carb': 'false',
                'low_fat': 'false',
                'low_sodium': 'false'
            }
        }


class DietOut(BaseModel):
    id: int
    balanced: bool
    high_fiber: bool
    high_protein: bool
    low_carb: bool
    low_fat: bool
    low_sodium: bool
    account_id_diet: int


class DietsOut(BaseModel):
    diets: list[DietOut]


class Health(BaseModel):
    id: int
    alcohol_cocktail: bool
    alcohol_free: bool
    celery_free: bool
    crustacean_free: bool
    dairy_free: bool
    dash: bool
    egg_free: bool
    fish_free: bool
    fodmap_free: bool
    gluten_free: bool
    immuno_supportive: bool
    keto_friendly: bool
    kidney_friendly: bool
    kosher: bool
    low_fat_abs: bool
    low_potassium: bool
    low_sugar: bool
    lupine_free: bool
    mediterranean: bool
    mollusk_free: bool
    mustard_free: bool
    no_oil_added: bool
    paleo: bool
    peanut_free: bool
    pescatarian: bool
    pork_free: bool
    red_meat_free: bool
    sesame_free: bool
    shellfish_free: bool
    soy_free: bool
    sugar_conscious: bool
    sulfite_free: bool
    tree_nut_free: bool
    vegan: bool
    vegetarian: bool
    wheat_free: bool
    account_id_health: int


class HealthIn(BaseModel):
    alcohol_cocktail: bool = False
    alcohol_free: bool = False
    celery_free: bool = False
    crustacean_free: bool = False
    dairy_free: bool = False
    dash: bool = False
    egg_free: bool = False
    fish_free: bool = False
    fodmap_free: bool = False
    gluten_free: bool = False
    immuno_supportive: bool = False
    keto_friendly: bool = False
    kidney_friendly: bool = False
    kosher: bool = False
    low_fat_abs: bool = False
    low_potassium: bool = False
    low_sugar: bool = False
    lupine_free: bool = False
    mediterranean: bool = False
    mollusk_free: bool = False
    mustard_free: bool = False
    no_oil_added: bool = False
    paleo: bool = False
    peanut_free: bool = False
    pescatarian: bool = False
    pork_free: bool = False
    red_meat_free: bool = False
    sesame_free: bool = False
    shellfish_free: bool = False
    soy_free: bool = False
    sugar_conscious: bool = False
    sulfite_free: bool = False
    tree_nut_free: bool = False
    vegan: bool = False
    vegetarian: bool = False
    wheat_free: bool = False

    class Config:
        schema_extra = {
            'example': {
                'alcohol_cocktail': 'false',
                'alcohol_free': 'false',
                'celery_free': 'false',
                'crustacean_free': 'false',
                'dairy_free': 'false',
                "dash": "false",
                "egg_free": "false",
                "fish_free": "false",
                "fodmap_free": "false",
                "gluten_free": "false",
                "immuno_supportive": "false",
                "keto_friendly": "false",
                "kidney_friendly": "false",
                "kosher": "false",
                "low_fat_abs": "false",
                "low_potassium": "false",
                "low_sugar": "false",
                "lupine_free": "false",
                "mediterranean": "false",
                "mollusk_free": "false",
                "mustard_free": "false",
                "no_oil_added": "false",
                "paleo": "false",
                "peanut_free": "false",
                "pescatarian": "false",
                "pork_free": "false",
                "red_meat_free": "false",
                "sesame_free": "false",
                "shellfish_free": "false",
                "soy_free": "false",
                "sugar_conscious": "false",
                "sulfite_free": "false",
                "tree_nut_free": "false",
                "vegan": "false",
                "vegetarian": "false",
                "wheat_free": "false"
            }
        }


class HealthOut(BaseModel):
    id: int
    alcohol_cocktail: bool
    alcohol_free: bool
    celery_free: bool
    crustacean_free: bool
    dairy_free: bool
    dash: bool
    egg_free: bool
    fish_free: bool
    fodmap_free: bool
    gluten_free: bool
    immuno_supportive: bool
    keto_friendly: bool
    kidney_friendly: bool
    kosher: bool
    low_fat_abs: bool
    low_potassium: bool
    low_sugar: bool
    lupine_free: bool
    mediterranean: bool
    mollusk_free: bool
    mustard_free: bool
    no_oil_added: bool
    paleo: bool
    peanut_free: bool
    pescatarian: bool
    pork_free: bool
    red_meat_free: bool
    sesame_free: bool
    shellfish_free: bool
    soy_free: bool
    sugar_conscious: bool
    sulfite_free: bool
    tree_nut_free: bool
    vegan: bool
    vegetarian: bool
    wheat_free: bool
    account_id_health: int


class HealthsOut(BaseModel):
    healths: list[HealthOut]


class DietQueries:
    def get_all_diets(self, account_id_diet: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id
                        , balanced
                        , high_fiber
                        , high_protein
                        , low_carb
                        , low_fat
                        , low_sodium
                        , account_id_diet
                        FROM diet
                        WHERE account_id_diet = %s
                        ORDER BY account_id_diet
                    """,
                    [account_id_diet]
                )
                result = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    result.append(record)

                return result

    def get_diet(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id
                        , balanced
                        , high_fiber
                        , high_protein
                        , low_carb
                        , low_fat
                        , low_sodium
                        , account_id_diet
                        FROM diet
                        WHERE account_id_diet = %s
                    """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def create_diet(self, diet: DietIn, account_id_diet: int) -> Diet:
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO diet (balanced
                        , high_fiber
                        , high_protein
                        , low_carb
                        , low_fat
                        , low_sodium
                        , account_id_diet
                    )
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id
                    """,
                    [
                        diet.balanced,
                        diet.high_fiber,
                        diet.high_protein,
                        diet.low_carb,
                        diet.low_fat,
                        diet.low_sodium,
                        account_id_diet
                    ],
                )
                id = result.fetchone()[0]
                return Diet(
                    id=id,
                    balanced=diet.balanced,
                    high_fiber=diet.high_fiber,
                    high_protein=diet.high_protein,
                    low_carb=diet.low_carb,
                    low_fat=diet.low_fat,
                    low_sodium=diet.low_sodium,
                    account_id_diet=account_id_diet,
                )

    def update_diet(self, account_id_diet, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.balanced,
                    data.high_fiber,
                    data.high_protein,
                    data.low_carb,
                    data.low_fat,
                    data.low_sodium,
                    account_id_diet
                ]
                cur.execute(
                    """
                    UPDATE diet
                    SET balanced = %s
                        , high_fiber = %s
                        , high_protein = %s
                        , low_carb = %s
                        , low_fat = %s
                        , low_sodium = %s
                    WHERE account_id_diet = %s
                    RETURNING id
                        , balanced
                        , high_fiber
                        , high_protein
                        , low_carb
                        , low_fat
                        , low_sodium
                        , account_id_diet
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def delete_diet(self, account_id_diet):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM diet
                    WHERE id = %s
                    """,
                    [account_id_diet],
                )


class HealthQueries:
    def get_all_healths(self, account_id_health: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id
                       , alcohol_cocktail
                        , alcohol_free
                        , celery_free
                        , crustacean_free
                        , dairy_free
                        , dash
                        , egg_free
                        , fish_free
                        , fodmap_free
                        , gluten_free
                        , immuno_supportive
                        , keto_friendly
                        , kidney_friendly
                        , kosher
                        , low_fat_abs
                        , low_potassium
                        , low_sugar
                        , lupine_free
                        , mediterranean
                        , mollusk_free
                        , mustard_free
                        , no_oil_added
                        , paleo
                        , peanut_free
                        , pescatarian
                        , pork_free
                        , red_meat_free
                        , sesame_free
                        , shellfish_free
                        , soy_free
                        , sugar_conscious
                        , sulfite_free
                        , tree_nut_free
                        , vegan
                        , vegetarian
                        , wheat_free
                        , account_id_health

                        FROM health
                        WHERE account_id_health = %s
                        ORDER BY account_id_health
                    """,
                    [account_id_health],
                )

                result = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    result.append(record)
                return result

    def get_health(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id
                       , alcohol_cocktail
                        , alcohol_free
                        , celery_free
                        , crustacean_free
                        , dairy_free
                        , dash
                        , egg_free
                        , fish_free
                        , fodmap_free
                        , gluten_free
                        , immuno_supportive
                        , keto_friendly
                        , kidney_friendly
                        , kosher
                        , low_fat_abs
                        , low_potassium
                        , low_sugar
                        , lupine_free
                        , mediterranean
                        , mollusk_free
                        , mustard_free
                        , no_oil_added
                        , paleo
                        , peanut_free
                        , pescatarian
                        , pork_free
                        , red_meat_free
                        , sesame_free
                        , shellfish_free
                        , soy_free
                        , sugar_conscious
                        , sulfite_free
                        , tree_nut_free
                        , vegan
                        , vegetarian
                        , wheat_free
                        , account_id_health

                        FROM health
                        WHERE account_id_health = %s
                    """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def create_health(self, health: HealthIn, account_id_health: int) -> Health:
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO health (alcohol_cocktail,
                                        alcohol_free,
                                        celery_free,
                                        crustacean_free,
                                        dairy_free,
                                        dash,
                                        egg_free,
                                        fish_free,
                                        fodmap_free,
                                        gluten_free,
                                        immuno_supportive,
                                        keto_friendly,
                                        kidney_friendly,
                                        kosher,
                                        low_fat_abs,
                                        low_potassium,
                                        low_sugar,
                                        lupine_free,
                                        mediterranean,
                                        mollusk_free,
                                        mustard_free,
                                        no_oil_added,
                                        paleo,
                                        peanut_free,
                                        pescatarian,
                                        pork_free,
                                        red_meat_free,
                                        sesame_free,
                                        shellfish_free,
                                        soy_free,
                                        sugar_conscious,
                                        sulfite_free,
                                        tree_nut_free,
                                        vegan,
                                        vegetarian,
                                        wheat_free,
                                        account_id_health)
                    VALUES (%s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s, %s, %s, %s,
                            %s)
                    RETURNING id
                    """,
                    [
                        health.alcohol_cocktail,
                        health.alcohol_free,
                        health.celery_free,
                        health.crustacean_free,
                        health.dairy_free,
                        health.dash,
                        health.egg_free,
                        health.fish_free,
                        health.fodmap_free,
                        health.gluten_free,
                        health.immuno_supportive,
                        health.keto_friendly,
                        health.kidney_friendly,
                        health.kosher,
                        health.low_fat_abs,
                        health.low_potassium,
                        health.low_sugar,
                        health.lupine_free,
                        health.mediterranean,
                        health.mollusk_free,
                        health.mustard_free,
                        health.no_oil_added,
                        health.paleo,
                        health.peanut_free,
                        health.pescatarian,
                        health.pork_free,
                        health.red_meat_free,
                        health.sesame_free,
                        health.shellfish_free,
                        health.soy_free,
                        health.sugar_conscious,
                        health.sulfite_free,
                        health.tree_nut_free,
                        health.vegan,
                        health.vegetarian,
                        health.wheat_free,
                        account_id_health
                    ],
                )
                id = result.fetchone()[0]
                return Health(
                    id=id,
                    alcohol_cocktail=health.alcohol_cocktail,
                    alcohol_free=health.alcohol_free,
                    celery_free=health.celery_free,
                    crustacean_free=health.crustacean_free,
                    dairy_free=health.dairy_free,
                    dash=health.dash,
                    egg_free=health.egg_free,
                    fish_free=health.fish_free,
                    fodmap_free=health.fodmap_free,
                    gluten_free=health.gluten_free,
                    immuno_supportive=health.immuno_supportive,
                    keto_friendly=health.keto_friendly,
                    kidney_friendly=health.kidney_friendly,
                    kosher=health.kosher,
                    low_fat_abs=health.low_fat_abs,
                    low_potassium=health.low_potassium,
                    low_sugar=health.low_sugar,
                    lupine_free=health.lupine_free,
                    mediterranean=health.mediterranean,
                    mollusk_free=health.mollusk_free,
                    mustard_free=health.mustard_free,
                    no_oil_added=health.no_oil_added,
                    paleo=health.paleo,
                    peanut_free=health.peanut_free,
                    pescatarian=health.pescatarian,
                    pork_free=health.pork_free,
                    red_meat_free=health.red_meat_free,
                    sesame_free=health.sesame_free,
                    shellfish_free=health.shellfish_free,
                    soy_free=health.soy_free,
                    sugar_conscious=health.sugar_conscious,
                    sulfite_free=health.sulfite_free,
                    tree_nut_free=health.tree_nut_free,
                    vegan=health.vegan,
                    vegetarian=health.vegetarian,
                    wheat_free=health.wheat_free,
                    account_id_health=account_id_health,

                )

    def update_health(self, account_id_health, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.alcohol_cocktail,
                    data.alcohol_free,
                    data.celery_free,
                    data.crustacean_free,
                    data.dairy_free,
                    data.dash,
                    data.egg_free,
                    data.fish_free,
                    data.fodmap_free,
                    data.gluten_free,
                    data.immuno_supportive,
                    data.keto_friendly,
                    data.kidney_friendly,
                    data.kosher,
                    data.low_fat_abs,
                    data.low_potassium,
                    data.low_sugar,
                    data.lupine_free,
                    data.mediterranean,
                    data.mollusk_free,
                    data.mustard_free,
                    data.no_oil_added,
                    data.paleo,
                    data.peanut_free,
                    data.pescatarian,
                    data.pork_free,
                    data.red_meat_free,
                    data.sesame_free,
                    data.shellfish_free,
                    data.soy_free,
                    data.sugar_conscious,
                    data.sulfite_free,
                    data.tree_nut_free,
                    data.vegan,
                    data.vegetarian,
                    data.wheat_free,
                    account_id_health
                ]
                cur.execute(
                    """
                    UPDATE health
                    SET  alcohol_cocktail = %s
                        , alcohol_free = %s
                        , celery_free = %s
                        , crustacean_free = %s
                        , dairy_free = %s
                        , dash = %s
                        , egg_free = %s
                        , fish_free = %s
                        , fodmap_free = %s
                        , gluten_free = %s
                        , immuno_supportive = %s
                        , keto_friendly = %s
                        , kidney_friendly = %s
                        , kosher = %s
                        , low_fat_abs = %s
                        , low_potassium = %s
                        , low_sugar = %s
                        , lupine_free = %s
                        , mediterranean = %s
                        , mollusk_free = %s
                        , mustard_free = %s
                        , no_oil_added = %s
                        , paleo = %s
                        , peanut_free = %s
                        , pescatarian = %s
                        , pork_free = %s
                        , red_meat_free = %s
                        , sesame_free = %s
                        , shellfish_free = %s
                        , soy_free = %s
                        , sugar_conscious = %s
                        , sulfite_free = %s
                        , tree_nut_free = %s
                        , vegan = %s
                        , vegetarian = %s
                        , wheat_free = %s

                    WHERE account_id_health = %s
                    RETURNING   id,
                                alcohol_cocktail,
                                alcohol_free,
                                celery_free,
                                crustacean_free,
                                dairy_free,
                                dash,
                                egg_free,
                                fish_free,
                                fodmap_free,
                                gluten_free,
                                immuno_supportive,
                                keto_friendly,
                                kidney_friendly,
                                kosher,
                                low_fat_abs,
                                low_potassium,
                                low_sugar,
                                lupine_free,
                                mediterranean,
                                mollusk_free,
                                mustard_free,
                                no_oil_added,
                                paleo,
                                peanut_free,
                                pescatarian,
                                pork_free,
                                red_meat_free,
                                sesame_free,
                                shellfish_free,
                                soy_free,
                                sugar_conscious,
                                sulfite_free,
                                tree_nut_free,
                                vegan,
                                vegetarian,
                                wheat_free,
                                account_id_health
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def delete_ingredient(self, account_id_health):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM health
                    WHERE id = %s
                    """,
                    [account_id_health],
                )
