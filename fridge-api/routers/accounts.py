from fastapi import (
    Depends,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel

from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountsOut,
    AccountsQueries,
)
from queries.preferences import DietQueries, DietIn, HealthQueries, HealthIn


class AccountForm(BaseModel):
    password: str
    username: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountsQueries = Depends(),
    diet_repo: DietQueries = Depends(),
    health_repo: HealthQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.hashed_password)
    account = repo.create(info, hashed_password)
    form = AccountForm(username=info.username, password=info.hashed_password)
    token = await authenticator.login(response, request, form, repo)
    diet_repo.create_diet(DietIn(), account.id)
    health_repo.create_health(HealthIn(), account.id)

    return AccountToken(account=account, **token.dict())


@router.get("/api/accounts", response_model=AccountsOut)
def accounts_list(queries: AccountsQueries = Depends()):
    return {
        "accounts": queries.get_all_accounts(),
    }


@router.delete("/api/account/{id}", response_model=bool)
def delete_account(id: int, queries: AccountsQueries = Depends()):
    queries.delete_account(id)
    return True


@router.get("/api/acccounts/{id}", response_model=AccountOut)
def get_account(
    id: int,
    response: Response,
    queries: AccountsQueries = Depends(),
):
    record = queries.get_account(id)
    if record is None:
        response.status_code = 404
    else:
        return record
