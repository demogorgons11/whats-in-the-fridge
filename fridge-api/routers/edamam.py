from fastapi import APIRouter, Depends
from queries.edamam import EdamamQueries
from authenticator import authenticator

router = APIRouter()


@router.get('/fridge-api/edamam/{id}')
def get_recipe(
    id: str,
    repo: EdamamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    requester = account_data["id"]
    return repo.get_recipe_by_id(id, requester)


@router.get('/fridge-api/edamam/search/{query}')
def get_recipes(
    query: str,
    repo: EdamamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    requester = account_data["id"]
    return repo.get_recipes_by_query(query, requester)
