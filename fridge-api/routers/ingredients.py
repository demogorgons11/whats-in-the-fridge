from fastapi import APIRouter, Depends, Response
from queries.ingredients import (
    IngredientIn,
    IngredientOut,
    IngredientsOut,
    IngredientsQueries,
)
from authenticator import authenticator

router = APIRouter()


@router.get("/api/ingredients", response_model=IngredientsOut)
def ingredients_list(
    queries: IngredientsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id_ingredient = account_data["id"]
    return {
        "ingredients": queries.get_all_ingredients(account_id_ingredient),
    }


@router.get("/api/ingredients/{id}", response_model=IngredientOut)
def get_ingredient(
    account_id_ingredient: int,
    response: Response,
    queries: IngredientsQueries = Depends(),
):
    record = queries.get_ingredient(account_id_ingredient)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.post("/api/ingredients/", response_model=IngredientOut)
def create_ingredient(
    ingredient_in: IngredientIn,
    queries: IngredientsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    requester = account_data["id"]
    return queries.create_ingredient(ingredient_in, requester)


@router.put("/api/ingredients/{id}", response_model=IngredientOut)
def update_ingredient(
    account_id_ingredient: int,
    ingredient_in: IngredientIn,
    response: Response,
    queries: IngredientsQueries = Depends(),
):
    record = queries.update_ingredient(account_id_ingredient, ingredient_in)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/ingredients/{id}", response_model=bool)
def delete_ingredient(id: int, queries: IngredientsQueries = Depends()):
    queries.delete_ingredient(id)
    return True
