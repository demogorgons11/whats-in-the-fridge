from fastapi import APIRouter, Depends, Response
from queries.preferences import (
    DietIn,
    DietOut,
    DietsOut,
    DietQueries,
    HealthIn,
    HealthOut,
    HealthQueries,
    HealthsOut,
)
from authenticator import authenticator


router = APIRouter()


@router.get("/api/diet", response_model=DietsOut)
def diets_list(
    queries: DietQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id_diet = account_data["id"]
    return {
        "diets": queries.get_all_diets(account_id_diet),
    }


@router.get("/api/diet/{account_id}", response_model=DietOut)
def get_diet(
    account_id: int,
    response: Response,
    queries: DietQueries = Depends(),
):
    record = queries.get_diet(account_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.post("/api/diet/", response_model=DietOut)
def create_diet(
    diet_in: DietIn,
    queries: DietQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    requester = account_data["id"]
    return queries.create_diet(diet_in, requester)


@router.put("/api/diet/", response_model=DietOut)
def update_diet(
    diet_in: DietIn,
    response: Response,
    queries: DietQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id_diet = account_data["id"]
    record = queries.update_diet(account_id_diet, diet_in)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/diet/{account_id}", response_model=bool)
def delete_diet(
    account_id: int,
    queries: DietQueries = Depends()
):
    queries.delete_diet(account_id)
    return True


@router.get("/api/health", response_model=HealthsOut)
def healths_list(
    queries: HealthQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id_health = account_data["id"]
    return {
        "healths": queries.get_all_healths(account_id_health),
    }


@router.get("/api/health/{id}", response_model=HealthOut)
def get_health(
    account_id: int,
    response: Response,
    queries: HealthQueries = Depends(),
):
    record = queries.get_health(account_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.post("/api/health/", response_model=HealthOut)
def create_health(
    health_in: HealthIn,
    queries: HealthQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    requester = account_data["id"]
    return queries.create_health(health_in, requester)


@router.put("/api/health/", response_model=HealthOut)
def update_health(
    health_in: HealthIn,
    response: Response,
    queries: HealthQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id_health = account_data["id"]
    record = queries.update_health(account_id_health, health_in)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/health/{id}", response_model=bool)
def delete_health(
    account_id: int,
    queries: DietQueries = Depends()
):
    queries.delete_diet(account_id)
    return True
