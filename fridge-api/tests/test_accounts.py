from fastapi.testclient import TestClient
from queries.accounts import AccountsQueries
from main import app

client = TestClient(app=app)


def get_current_account_data_mock():
    return {
        'id': 11,
        'username': "test_username",
        'hashed_password': "test_password"
    }


class AccountsQueriesMock:
    def get_all_accounts(self):
        return []


def test_get_all_accounts():
    app.dependency_overrides[AccountsQueries] = AccountsQueriesMock
    res = client.get('/api/accounts')
    assert res.status_code == 200
    assert res.json() == {'accounts': []}
    app.dependency_overrides = {}
