from fastapi.testclient import TestClient
from queries.preferences import DietQueries
from main import app
from authenticator import authenticator

client = TestClient(app=app)


def get_current_account_data_mock():
    return {
        'id': 11,
        'username': "test_username",
        'hashed_password': "test_password"
    }


class DietQueriesMock:
    def get_all_diets(self, account_id_diet: int):
        return []


def test_get_all_diets():
    app.dependency_overrides[DietQueries] = DietQueriesMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    res = client.get('/api/diet')
    assert res.status_code == 200
    assert res.json() == {'diets': []}
    app.dependency_overrides = {}
