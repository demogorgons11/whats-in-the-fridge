import json
from fastapi.testclient import TestClient
from queries.ingredients import IngredientsQueries
from routers.ingredients import IngredientIn, IngredientOut
from authenticator import authenticator
from main import app

client = TestClient(app=app)


def get_current_account_data_mock():
    return {
        'id': 7,
        'username': "test_name"
    }


class IngredientsQueriesMock:
    def get_all_ingredients(self, account_id_ingredient: int):
        return []

    def create_ingredient(
        self,
        ingredient: IngredientIn,
        account_id_ingredient: int
    ) -> IngredientOut:
        ingredient_dict = ingredient.dict()
        return IngredientOut(
            id=1337,
            account_id_ingredient=account_id_ingredient,
            **ingredient_dict
            )


def test_create_ingredient():
    # Arrange
    app.dependency_overrides[IngredientsQueries] = IngredientsQueriesMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    ingredient_body = {
        'name': "test_ingredient",
        'quantity': 42,
        'account_id_ingredient': 1,
    }

    res = client.post('/api/ingredients/', json.dumps(ingredient_body))

    assert res.status_code == 200
    assert res.json()['id'] == 1337
    assert res.json()['account_id_ingredient'] == 7

    app.dependency_overrides = {}


def test_get_all_ingredients():
    app.dependency_overrides[IngredientsQueries] = IngredientsQueriesMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock

    res = client.get('/api/ingredients')

    assert res.status_code == 200
    assert res.json() == {'ingredients': []}

    app.dependency_overrides = {}
