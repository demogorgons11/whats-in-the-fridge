import { Routes, Route } from "react-router-dom";
import "./App.css";
import IngredientForm from "./IngredientForm";
import MainPage from "./MainPage";
import Ingredients from "./Ingredients";
import CreatePreferences from "./Preferences";

function App() {
  return (
    <div className="container-app">
      <Routes>
        <Route path="/api/ingredients/" element={<IngredientForm />} />
        <Route path="/" element={<MainPage />} />
        <Route path="/ingredients/" element={<Ingredients />} />
        <Route path="/preferences/" element={<CreatePreferences />} />
      </Routes>
    </div>
  );
}

export default App;
