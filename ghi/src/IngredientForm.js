import { useNavigate } from "react-router-dom";
import { useAddIngredientMutation } from "./store/api";
import { useState } from "react";
import ErrorNotification from "./ErrorNotification";
import "./IngredientForm.css";

function IngredientForm() {
  const navigate = useNavigate();
  const [addIngredient, result] = useAddIngredientMutation();
  const [error, setError] = useState("");
  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState("");
  const [expiration_date, setExpirationDate] = useState("");
  const [sent, setSent] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();
    addIngredient({ name, quantity, expiration_date });
    setName("");
    setQuantity("");
    setExpirationDate("");
    setSent(true);
    setTimeout(() => {
      setSent(false);
    }, 2500);
  }

  if (result.isError) {
    setError(result.error);
  }

  return (
    <div className="body">
      <div className="ingredient-form-container">
        <div className="box">
          {sent && (
            <div
              style={{ position: "fixed", height: "50px", width: "280px" }}
              className="alert alert-success fade show"
            >
              Ingredient added!
            </div>
          )}
          <div className="content">
            <ErrorNotification error={error} />
            <h1>Add to your fridge</h1>

            <form
              onSubmit={handleSubmit}
              style={{
                display: "table-cell",
                textAlign: "left",
              }}
            >
              <div className="field">
                <label className="label">Ingredient Name</label>
                <div className="control">
                  <input
                    id="Name"
                    name="name"
                    required
                    className="input"
                    type="text"
                    placeholder="Ingredient name"
                    value={name}
                    onChange={(event) => {
                      setName(event.target.value);
                    }}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Quantity</label>
                <div className="control">
                  <input
                    id="Quantity"
                    name="quantity"
                    required
                    className="input"
                    type="text"
                    placeholder="Quantity"
                    value={quantity}
                    onChange={(event) => {
                      setQuantity(event.target.value);
                    }}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Expiration Date (optional)</label>
                <div className="control">
                  <input
                    id="Expiration Date"
                    name="expiration_date"
                    className="input"
                    type="date"
                    value={expiration_date}
                    onChange={(event) => {
                      setExpirationDate(event.target.value);
                    }}
                  />
                </div>
              </div>
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-primary">Add Ingredient</button>
                </div>
                <div className="control">
                  <button
                    onClick={() => navigate("/api/ingredients/")}
                    type="button"
                    className="button"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IngredientForm;
