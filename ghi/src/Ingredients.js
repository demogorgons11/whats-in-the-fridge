import {
  useGetIngredientsQuery,
  useDeleteIngredientMutation,
  useGetRecipesMutation,
} from "./store/api";
import { useNavigate } from "react-router-dom";
import "./ingredients.css";
import React, { useState, useEffect } from "react";
import ErrorNotification from "./ErrorNotification";

let query = " ";

export default function Ingredients() {
  const { data: ingredientData, isLoading: isIngredientLoading } =
    useGetIngredientsQuery();
  const [getRecipes, results] = useGetRecipesMutation();
  const [userinfo, setUserInfo] = useState({ ingredients: [], response: [] });
  const [recipeData, setRecipeData] = useState([]);
  const navigate = useNavigate();
  const [deleteIngredient] = useDeleteIngredientMutation();
  const [currentDate, setCurrentDate] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    setCurrentDate(year + "-" + month + "-" + date);
  }, []);

  const handleChange = async (e) => {
    const { value, checked } = e.target;
    const { ingredients } = userinfo;

    if (checked) {
      setUserInfo({
        ingredients: [...ingredients, value],
        response: [...ingredients, value],
      });
      query = query.concat(" ", value);
      let data = await getRecipes(query);
      setRecipeData(data["data"]);
    } else {
      setUserInfo({
        ingredients: ingredients.filter((e) => e !== value),
        response: ingredients.filter((e) => e !== value),
      });
      query = query.replace(value, "");
      if (query.includes(" ")) {
        query = query.concat("%20");
      }
      let data = await getRecipes(query);
      setRecipeData(data["data"]);
    }
  };

  if (results.isError) {
    setError(results.error);
  }

  if (isIngredientLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  return (
    <body style={{ backgroundImage: "none" }}>
      <div className="dashboard-container">
        <ErrorNotification error={error} />
        <div className="left-container">
          <div className="left-title">
            <h1>
              <span>
                What's in your Fridge?
                <img
                  src={require("./images/fridge.png")}
                  className="h-10 mx-2 ml-1"
                  alt="fridge"
                  style={{ height: "60px", width: "60px" }}
                />
              </span>
            </h1>
          </div>
          <table className="table">
            <thead>
              <tr
                style={{
                  backgroundColor: "#c47d66"
                }}
              >
                <th style={{ color: "white" }}>I want to use...</th>
                <th style={{ color: "white" }}>Name</th>
                <th style={{ color: "white" }}>Quantity</th>
                <th style={{ color: "white" }}>Expiration Date</th>
                <th style={{ color: "white" }}>Delete</th>
              </tr>
            </thead>
            <tbody>
              {ingredientData?.ingredients.map((ingredient) => (
                <tr key={ingredient.id} className="row-hover">
                  <td>
                    <input
                      className="option-input checkbox"
                      type="checkbox"
                      name="ingredients"
                      value={ingredient.name}
                      id="flexCheckDefault"
                      onChange={handleChange}
                    />
                  </td>
                  <td> {ingredient.name}</td>
                  <td>{ingredient.quantity}</td>
                  {!ingredient.expiration_date && (
                    <td>{ingredient.expiration_date}</td>
                  )}
                  {Date.parse(currentDate) >
                    Date.parse(ingredient.expiration_date) && (
                    <td style={{ color: "red" }}>Expired!</td>
                  )}
                  {Date.parse(currentDate) <
                    Date.parse(ingredient.expiration_date) && (
                    <td>{ingredient.expiration_date}</td>
                  )}
                  <td>
                    <button
                      className="delete-btn m-2 float-right"
                      onClick={() => {
                        deleteIngredient(ingredient.id);
                        navigate(0);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div className="right-container">
        <div className="right-title">
          <h1>
            What can you make?
            <img
              src={require("./images/cooking.png")}
              className="h-10 mx-2 ml-1"
              alt="what-can-you-make"
              style={{ height: "60px", width: "60px" }}
            />
          </h1>
        </div>
        <div className="row">
          <div className="recipe-cards"></div>
          {recipeData?.map((recipe) => (
            <div key={recipe[2]} className="col-sm-auto mp-4">
              <div
                className="card"
                style={{
                  display: "grid",
                  gridTemplateColumns: "21ch",
                  gridGap: 2,
                }}
              >
                <img src={recipe[1]} className="card-img-top" alt="..." />
                <div className="card-body">
                  <h5 className="card-title">
                    <strong>{recipe[0]}</strong>
                  </h5>
                  <button className="link-button">
                    <a href={recipe[2]}>Take me to the recipe!</a>
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </body>
  );
}
