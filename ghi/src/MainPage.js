import "./MainPage.css";
import "./what-the-fridge-high-resolution-color-logo.png";

function MainPage() {
  return (
    <div className="body">
      <div className="px-4 py-5 text-center">
        <h1>
          <img
            alt=""
            src={require("./what-the-fridge-high-resolution-color-logo.png")}
            style={{ width: 400, height: 400, borderRadius: "20px" }}
          ></img>
        </h1>
        <div className="col-lg-6 mx-auto"></div>
      </div>
    </div>
  );
}
export default MainPage;
