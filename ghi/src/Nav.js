import { NavLink } from "react-router-dom";
import { useEffect } from "react";
import { useGetTokenQuery, useLogOutMutation } from "./store/api";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { showModal, LOG_IN_MODAL, SIGN_UP_MODAL } from "./store/accountSlice";
import LogInModal from "./modals/LoginModal";
import SignUpModal from "./modals/SignUpModal";
import "./nav.css";
import "./what-the-fridge-high-resolution-color-logo.png";

function LoginButtons(props) {
  const dispatch = useDispatch();
  const classNames = `buttons ${props.show ? "" : "is-hidden"}`;

  return (
    <div className={classNames}>
      <button
        onClick={() => dispatch(showModal(SIGN_UP_MODAL))}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
        }}
      >
        Sign up
      </button>
      <button
        onClick={() => dispatch(showModal(LOG_IN_MODAL))}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
        }}
      >
        Log in
      </button>
    </div>
  );
}

function LogoutButton() {
  const navigate = useNavigate();
  const [logOut, { data }] = useLogOutMutation();

  useEffect(() => {
    if (data) {
      navigate("/");
    }
  }, [data, navigate]);

  return (
    <div className="buttons">
      <button
        onClick={logOut}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
          marginRight: "5px",
        }}
      >
        Log out
      </button>
    </div>
  );
}

function CreateIngredientButton() {
  const navigate = useNavigate();

  return (
    <div className="buttons">
      <button
        onClick={() => {
          navigate("/api/ingredients/");
        }}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
          marginRight: "5px",
        }}
      >
        Add Ingredient
      </button>
    </div>
  );
}
function IngredientsListButton() {
  const navigate = useNavigate();

  return (
    <div className="buttons">
      <button
        onClick={() => {
          navigate("/ingredients/");
          window.location.reload(false);
        }}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
          marginRight: "5px",
        }}
      >
        Your Fridge
      </button>
    </div>
  );
}

function PreferencesButton() {
  const navigate = useNavigate();

  return (
    <div className="buttons">
      <button
        onClick={() => {
          navigate("/preferences/");
        }}
        className="button"
        style={{
          backgroundColor: "#c47d66",
          color: "white",
          marginRight: "5px",
        }}
      >
        Your Preferences
      </button>
    </div>
  );
}

function Nav() {
  const { data: token, isLoading: tokenLoading } = useGetTokenQuery();

  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-dark"
        style={{
          backgroundColor: "#003366",
        }}
      >
        <div className="container-fluid nav-color">
          <NavLink className="navbar-brand" to="/">
            <img
              alt=""
              src={require("./what-the-fridge-high-resolution-color-logo.png")}
              width="150"
              height="150"
            ></img>
          </NavLink>
          <div className="divider">
            <p className="lead mb-4">
              <strong
                style={{
                  color: "white",
                  fontSize: "20px",
                  fontFamily: "Source Code Pro, monospace",
                }}
              >
                Don't say what the f@$&?
                <p style={{ color: "#c47d66" }}>Say what the fridge!</p>
              </strong>
            </p>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              {tokenLoading ? (
                <LoginButtons show={false} />
              ) : token ? (
                <>
                  <div>
                    <CreateIngredientButton />
                  </div>
                  <div>
                    <IngredientsListButton />
                  </div>
                  <div>
                    <PreferencesButton />
                  </div>
                  <div>
                    <LogoutButton />
                  </div>
                </>
              ) : (
                <LoginButtons show={true} />
              )}
            </div>
          </div>
        </div>
      </nav>
      <LogInModal />
      <SignUpModal />
    </>
  );
}

export default Nav;
