import {
  useGetDietQuery,
  useUpdateDietMutation,
  useGetHealthQuery,
  useUpdateHealthMutation,
} from "./store/api";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import ErrorNotification from "./ErrorNotification";
import "./preferences.css";

export default function CreatePreferences() {
  const { data: dietData, isLoading: isLoadingDiet } = useGetDietQuery();
  const { data: healthData, isLoading: isLoadingHealth } = useGetHealthQuery();
  const [userDiet, setUserDiet] = useState({ diets: [], response: [] });
  const [userHealth, setUserHealth] = useState({ healths: [], response: [] });
  const [updateDiet, dietResult] = useUpdateDietMutation();
  const [updateHealth, healthResult] = useUpdateHealthMutation();
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleDiet = async (e) => {
    const { value, checked } = e.target;
    const { diets } = userDiet;

    if (checked) {
      setUserDiet({
        diets: [...diets, value],
        response: [...diets, value],
      });
      let data = Object.entries(dietData["diets"][0]);
      data.pop();
      data.shift();
      let dietObj = {};
      for (let arr of data) {
        dietObj[arr[0]] = arr[1];
        if (arr[0] === value) {
          dietObj[arr[0]] = true;
        }
      }
      updateDiet(dietObj);
      navigate(0);
    } else {
      setUserDiet({
        diets: diets.filter((e) => e !== value),
        response: diets.filter((e) => e !== value),
      });
      let data = Object.entries(dietData["diets"][0]);
      data.pop();
      data.shift();
      let dietObj = {};
      for (let arr of data) {
        dietObj[arr[0]] = arr[1];
        if (arr[0] === value) {
          dietObj[arr[0]] = false;
        }
      }
      updateDiet(dietObj);
      navigate(0);
    }
  };

  const handleHealth = async (e) => {
    const { value, checked } = e.target;
    const { healths } = userHealth;

    if (checked) {
      setUserHealth({
        healths: [...healths, value],
        response: [...healths, value],
      });
      let data = Object.entries(healthData["healths"][0]);
      data.pop();
      data.shift();
      let healthObj = {};
      for (let arr of data) {
        healthObj[arr[0]] = arr[1];
        if (arr[0] === value) {
          healthObj[arr[0]] = true;
        }
      }
      updateHealth(healthObj);
      navigate(0);
    } else {
      setUserHealth({
        healths: healths.filter((e) => e !== value),
        response: healths.filter((e) => e !== value),
      });
      let data = Object.entries(healthData["healths"][0]);
      data.pop();
      data.shift();
      let healthObj = {};
      for (let arr of data) {
        healthObj[arr[0]] = arr[1];
        if (arr[0] === value) {
          healthObj[arr[0]] = false;
        }
      }
      updateHealth(healthObj);
      navigate(0);
    }
  };

  if (dietResult.isError) {
    setError(dietResult.error);
  }

  if (healthResult.isError) {
    setError(healthResult.error);
  }

  if (isLoadingDiet) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  if (isLoadingHealth) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  return (
    <div className="container">
      <ErrorNotification error={error} />
      <div className="left-half">
        <table className="table">
          <thead>
            <tr>
              <th
                style={{
                  backgroundColor: "#c47d66",
                  position: "sticky",
                  fontSize: "30px",
                  width: "195px",
                  height: "30px",
                }}
              >
                Diet{" "}
                <img
                  src={require("./images/diet.png")}
                  className="h-10 mx-2 ml-1"
                  alt="fridge"
                  style={{ height: "60px", width: "60px" }}
                />
              </th>
            </tr>
          </thead>
          <tbody>
            {Object.entries(dietData["diets"][0])
              .slice(1, 7)
              ?.map((diet) => (
                <tr key={diet[0]}>
                  <td>
                    {diet[1] === true && (
                      <input
                        className="option-input checkboxes"
                        type="checkbox"
                        name="diets"
                        value={diet[0]}
                        id="flexCheckDefault"
                        onChange={handleDiet}
                        checked
                      />
                    )}

                    {diet[1] === false && (
                      <input
                        className="option-input checkboxes"
                        type="checkbox"
                        name="diets"
                        value={diet[0]}
                        id="flexCheckDefault"
                        onChange={handleDiet}
                      />
                    )}
                  </td>
                  <td>{diet[0]}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>

      <div className="right-half">
        <table className="table">
          <thead>
            <tr
              className="health-table"
              style={{
                backgroundColor: "#c47d66",
                position: "sticky",
                width: "100%",
              }}
            >
              <th
                style={{
                  backgroundColor: "#c47d66",
                  position: "sticky",
                  fontSize: "30px",
                  width: "225px",
                  height: "30px",
                }}
              >
                Health
                <img
                  src={require("./images/healthy.png")}
                  className="h-10 mx-2 ml-1"
                  alt="fridge"
                  style={{ height: "60px", width: "60px" }}
                />
              </th>
            </tr>
          </thead>
          <tbody>
            {Object.entries(healthData["healths"][0])
              .slice(1, 37)
              ?.map((health) => (
                <tr key={health[0]}>
                  <td>
                    {health[1] === true && (
                      <input
                        className="option-input checkboxes"
                        type="checkbox"
                        name="healths"
                        value={health[0]}
                        id="flexCheckDefault"
                        onChange={handleHealth}
                        checked
                      />
                    )}

                    {health[1] === false && (
                      <input
                        className="option-input checkboxes"
                        type="checkbox"
                        name="healths"
                        value={health[0]}
                        id="flexCheckDefault"
                        onChange={handleHealth}
                      />
                    )}
                  </td>
                  <td>{health[0]}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
