import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLogInMutation } from "../store/api";
import { eventTargetSelector as target, preventDefault } from "../store/utils";
import { showModal, updateField, LOG_IN_MODAL } from "../store/accountSlice";
import Notification from "../Notification";
import "./Modals.css";

function LogInModal() {
  const dispatch = useDispatch();
  const { show, username, password } = useSelector((state) => state.account);
  const modalClass = `modal ${show === LOG_IN_MODAL ? "is-active" : ""}`;
  const [logIn, { error, isLoading: logInLoading }] = useLogInMutation();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  return (
    <div className={modalClass} key="login-modal">
      <div className="modal-background"></div>
      <div className="modal-content" style={{ width: "300px" }}>
        <div className="box content">
          <div className="modal-body">
            {error ? (
              <Notification type="danger">{error.data.detail}</Notification>
            ) : null}
            <div className="left-side">
              <form method="POST" onSubmit={preventDefault(logIn, target)}>
                <h3>Welcome back!</h3>
                <div className="username-field">
                  <div className="control">
                    <input
                      required
                      onChange={field}
                      value={username}
                      name="username"
                      className="input"
                      type="text"
                      placeholder="username"
                    />
                  </div>
                </div>
                <div className="password-field">
                  <div className="control">
                    <input
                      required
                      onChange={field}
                      value={password}
                      name="password"
                      className="input"
                      type="password"
                      placeholder="secret password..."
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="control">
                    <button disabled={logInLoading} className="login-button">
                      Login
                    </button>
                  </div>
                  <div className="control">
                    <button
                      type="button"
                      onClick={() => dispatch(showModal(null))}
                      className="cancel-button"
                    >
                      Cancel
                    </button>
                  </div>
                </div>
                <div className="picture-container">
                  <img
                    alt=""
                    src={require("./image/checking.gif")}
                    style={{ height: "200px", width: "200px" }}
                  ></img>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LogInModal;
