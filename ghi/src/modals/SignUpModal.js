import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSignUpMutation } from "../store/api";
import { preventDefault } from "../store/utils";
import { showModal, updateField, SIGN_UP_MODAL } from "../store/accountSlice";
import Notification from "../Notification";
import "./Modals.css";

function SignUpModal() {
  const dispatch = useDispatch();
  const { show, username, password } = useSelector((state) => state.account);
  const modalClass = `modal ${show === SIGN_UP_MODAL ? "is-active" : ""}`;
  const [signUp, { error, isLoading: signUpLoading }] = useSignUpMutation();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  return (
    <div className={modalClass} key="signup-modal">
      <div className="modal-background"></div>
      <div className="modal-content" style={{ width: "300px" }}>
        <div className="box content">
          <div className="modal-body">
            {error ? (
              <Notification type="danger">Invalid Username</Notification>
            ) : null}
            <div className="left-side">
              <h3>Sign Up</h3>
              <form
                method="POST"
                onSubmit={preventDefault(signUp, () => ({
                  username: username,
                  hashed_password: password,
                }))}
              >
                <div className="field">
                  <label className="label" htmlFor="username" hidden>
                    Username
                  </label>
                  <div className="control">
                    <input
                      required
                      onChange={field}
                      value={username}
                      name="username"
                      className="input"
                      type="text"
                      placeholder="create username here"
                    />
                  </div>
                </div>
                <div className="field">
                  <label className="label" hidden>
                    Password
                  </label>
                  <div className="control">
                    <input
                      required
                      onChange={field}
                      value={password}
                      name="password"
                      className="input"
                      type="password"
                      placeholder="secret password..."
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="control">
                    <button disabled={signUpLoading} className="create-button">
                      Create
                    </button>
                  </div>
                  <div className="control">
                    <button
                      type="button"
                      onClick={() => dispatch(showModal(null))}
                      className="cancel-button"
                    >
                      Cancel
                    </button>
                  </div>
                </div>
                <div className="picture-container">
                  <img
                    alt=""
                    src={require("./image/cooking.gif")}
                    style={{ height: "200px", width: "200px" }}
                  ></img>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpModal;
