import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { clearForm } from "./accountSlice";

export const apiSlice = createApi({
  reducerPath: "accounts",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SAMPLE_SERVICE_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = apiSlice.endpoints.getToken.select();
      const { data: tokenData } = selector(getState());
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["Account", "Ingredient", "Token", "Recipe", "Diet", "Health"],
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (data) => ({
        url: "/api/accounts",
        method: "post",
        body: data,
        credentials: "include",
      }),
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token", "Ingredient", "Recipe"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),
    logIn: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.username);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token", "Ingredient", "Recipe"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),
    logOut: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: [
        "Account",
        "Token",
        "Ingredient",
        "Recipe",
        "Diet",
        "Health",
      ],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["Token"],
    }),
    addIngredient: builder.mutation({
      query: (form) => {
        if (form.expiration_date === "") {
          form.expiration_date = null;
        }
        return {
          method: "post",
          url: "/api/ingredients/",
          credentials: "include",
          body: form,
        };
      },
      invalidatesTags: ["Ingredient"],
    }),
    getIngredients: builder.query({
      query: () => ({
        url: "/api/ingredients",
        credentials: "include",
      }),
      providesTags: ["Ingredient"],
    }),
    deleteIngredient: builder.mutation({
      query: (ingredientId) => ({
        method: "delete",
        url: `/api/ingredients/${ingredientId}`,
      }),
      invalidatesTags: ["Ingredient", "Recipe"],
    }),
    getRecipes: builder.mutation({
      query: (data) => ({
        method: "get",
        url: `/fridge-api/edamam/search/${data}`,
        credentials: "include",
      }),
      providesTags: ["Recipe"],
      invalidatesTags: ["Recipe"],
    }),
    getDiet: builder.query({
      query: () => ({
        url: "/api/diet",
        credentials: "include",
      }),
      providesTags: ["Diet"],
    }),
    updateDiet: builder.mutation({
      query: (data) => ({
        method: "put",
        url: "/api/diet/",
        credentials: "include",
        body: data,
      }),
      providesTags: ["Diet"],
    }),
    getHealth: builder.query({
      query: () => ({
        url: "/api/health",
        credentials: "include",
      }),
      providesTags: ["Health"],
    }),
    updateHealth: builder.mutation({
      query: (data) => ({
        method: "put",
        url: "/api/health/",
        credentials: "include",
        body: data,
      }),
      providesTags: ["Health"],
    }),
  }),
});

export const {
  useAddIngredientMutation,
  useGetIngredientsQuery,
  useGetTokenQuery,
  useLogInMutation,
  useLogOutMutation,
  useDeleteIngredientMutation,
  useSignUpMutation,
  useGetRecipesMutation,
  useGetDietQuery,
  useUpdateDietMutation,
  useGetHealthQuery,
  useUpdateHealthMutation,
} = apiSlice;
