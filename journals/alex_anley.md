## Jan 26, 2023

-Found a bug in the recipes filter search query and attempting deployment

Our search query was not working as expected. If a current user was filtering recipes, the query was storing their ingredients even after the user logged out. So when a new user logged in, the recipe filter was including recipes with the previous user’s checked ingredients. We were able to resolve this by refreshing the page when a user clicks on the fridge button. Phew! 

## Jan 25, 2023

-Front end aesthetic changes 

Today we were adding/changing minor front end components to enhance user experience. For example, if an ingredient is close to expiring in a user’s fridge, it will display a warning in red. Additionally, after a user adds an ingredient and the form refreshes, it displays a confirmation message that the ingredient was added successfully. We also created a gif to display on the homepage that shows a user how to use the app. Lastly, we changed the font color on some of the table headers to match the button colors across the app. 

## Jan 24, 2023

-Finish setting up preferences page to include diet and health preferences

This was a challenging day and I spent most of it observing and learning from Jeremy. He set up the user preferences page on the front end to display all of the different health and diet preferences/restrictions that a user can add or delete. Everything is working as expected and I am very impressed with his work and learned a lot!

## Jan 23, 2023

-Unit testing

Today, I wrote my first unit test and also fixed a test that was not working as expected. I wrote a unit test to get a list of all accounts and fixed the unit test in order to get a list of all ingredients by user :)

## Jan 21, 2023

-Filtered recipes! 

This was the most exciting day, by far! We have our recipes filtering as expected. When a user selects on checkbox, the recipes filter on the right hand side of the page based upon that ingredient. After another checkbox is selected, the recipes filter by those two selected ingredients, and so on and so forth. If a checkbox is removed, the filtered recipes reset, also. 

## Jan 20, 2023

-Set up checkboxes for ingredients 

I was very proud of myself for accomplishing this! We decided that, in order for the recipes to filter by ingredients, we wanted to set up the ingredients list to have checkboxes for each individual ingredient. After a checkbox is selected, the recipes would then filter based on the ingredient(s) that a user wants to use. Filtering is not set up yet, but the checkboxes are working as expected!

## Jan 19, 2023

-Split user dashboard and set up card layout for recipes

Today, we split the ingredients page so that the list of a user’s ingredients is on the left hand side of the page, with a separate scroll bar. On the right hand side of the page, we set up to display the recipe cards. We do not have it filtering it just yet, but we hard coded one recipe to make sure the card layout/format was what we wanted visually. 


## Jan 17, 2023

-Create an ingredient form still in progress

Today, we worked on the add an ingredient form. The form is functional, however, we have been trying to make the data clear after the form has been submitted. Other than that, everything else works as expected. We fixed the quantity and expiration date errors and also fixed the button for add an ingredient in the navbar to redirect to the appropriate page. 


## Jan 11-15, 2023

-Setting up login modal, sign up modal and front end authentication

This was by far the most challenging part of the project so far. I am very grateful that Corey and I were able to accomplish this. First, we had to set up front end authentication using Redux. In order to do that, we went back to all of the Redux videos and documentation in order to do this. We also used the cookbook, as well. After this, we created a login and sign up modal and logout button for users to login, signup and logout on the front end. This took all day Friday and many hours over the weekend to accomplish- but we did it, yay!

## Jan 10, 2023

-Finished foreign key set up to sync data from account_id to user ingredients table

Like JWTDown authentication and database set up, this was very challenging! We really struggled to figure out how to connect the account and ingredients table using account ID as a foreign key. Finally, we looked back into some sample code in the live channel and were able to figure it out. Now, every time a user creates an ingredient, it syncs to their account id as expected. 

## Jan 9, 2023

-Integrated 3rd party API “Edamam”

Today was a breakthrough day! We configured the queries and routes for our 3rd party API, called Edamam, and set up endpoints to search for recipes by ingredients and also search for recipes using an ID. We tested this many times in Swagger, using different and multiple ingredients and everything is working as expected :)

## Jan 6, 2023

-Continuing authentication set up

Today, we continued working on the JWTdown back end authentication. After the lecture, we were able to set it up successfully. I experienced great joy when this issue was complete. 

## Jan 5, 2023

-Setting up JWTdown authentication

Today, we started setting up the authentication for our application. Like the database set up, it is a lot more challenging than I thought it would be! Currently, the error I am getting is about a circular import. Our group has decided to wait until Friday’s lecture on authentication before moving forward.

## Jan 4, 2023

-Setting up the PostgreSQL database to accept and display data

Today we set up our database. It was a lot more challenging than I thought it would be! First, we tried setting up our yaml file to create multiple databases. We also used the instructions on Learn to create multiple databases, too. After several unsuccessful attempts, we realized we only needed one database for our application. Therefore, we deleted the relational databases directory and changed our yaml file to set up one single PostgreSQL database. And alas, we got it up and running! 

## Jan 3, 2023

-Testing the Edamam API which is the 3rd party API we are using as the basis for our application

Today we tested out the recipe search function. We discovered that we can search for recipes in Edamam that includes all of the ingredients listed in the search. We narrowed down what we need to extract from the API (ingredients, recipe name, image, etc) and also discussed how to set up the health and diet preferences for each individual user. 
