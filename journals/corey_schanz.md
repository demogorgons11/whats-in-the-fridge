1/26/23
Today we found a bug where the ingredients from a previous user were getting stored in the query for a new user. We were able to get this issue resolved and we started working on deployment.

1/25/23
Today we worked on some minor user experience features that we thought would add to the project. I added an error message to show up if you tried to create a user with a duplicate user name.

1/24/23
Today we got the user preferences page set up and working correctly. Jeremy did most of the driving and he killed it. The recipes are now filtered by any preferences the user adds to their profile.

1/23/23
Today I wrote a unit test to get all the diets. This was my first unit test and I was able to get it to pass successfully.

1/21/23
Today we got our checkboxes on ingredients to filter the recipes list that gets returned from our 3rd party API. This is a major step for us since it is basically the whole idea of our project.

1/20/23
Today we worked on setting up a checkbox next to each ingredient in the table. We want the checkboxes to filter the search for recipes. It's not working yet, but we are getting the values we want from the checkboxes.

1/19/23
Today we successfully got our dashboard page split into two parts so that it shows the ingredients that you have in your fridge display on the left and the recipe cards will display on the right.

1/17/23
Today we got an add ingredient form set up. It works how we want it to and we have a button in the navbar that takes you to the page. We are still struggling to get the form to clear when you hit the submit button.

1/11/23-1/15/23
Alex and I spent this entire time trying to get the front end authentication set up. It was extremely challenging. We were advised to use redux and we watched Curtis's video several times to get this set up. We also used the example code from the lecture as a reference. After a long time struggling, we got it done.

1/10/23
Today we set up a foreign key sync our data from the account id to the ingredients table. We struggled mightily. We had to look at some of the example code that was posted in the live channel to figure it out, but we got it eventually.

1/9/23
Today we set up our 3rd party integration. We have tested it all in Swagger and everything is working the way we want it to. We can search recipes and filter it by ingredients and it requieres an ID.

1/6/23
Still working on JWTdown authentication. Once we had a chance to hear the lecture, we got it working.

1/5/23
Today we were working on JWTdown authentication. We are having a lot of issues. We have decided as a group to wait for the lecture and try it after we have a better understanding of it.

1/4/23
We spent most of the day trying to set up our docker compose YAML file so that we could get everything up and running. We have our React construction page so that it's visible now and we can connect to swagger. Alex and I kept trying to get the database to work until about 7:15. We finally got it.
