1/3/2023: After feedback from Riley, we’ve reassessed some of the capabilities of the project. We’re tabling the Shopping List feature as a stretch goal for the time being, and also after playing with the Edamam API more we decided to add checkboxes on the Pantry/Fridge sidebar for picking specific ingredients to filter by.

1/5/2023: We decided to stick with PostgreSQL instead of MongoDB. We're setting up tables now, and the hardest part is trying to manage all of the different diet and health options. Right now my first thought is to make a separate diet and health table and tie them to a user.

1/6/2023: Today was spent getting auth working. We're using JWTdown and the documentation, but we've run into a few issues. I think I'm understanding how to authenticate the routes now, though.

1/9/2023: Today is the big day where we try to actually get the Edamam search to work with our website. If we can't get this working we'll have to completely rethink how we're going to do this project, but I'm optimistic. We know we can get the data, so its all about creating a route so that we can use the data.

1/11/2023: We split into two groups, and Yvette and I are tackling the preferences page. I've got some ideas for how to handle things, but I think the code is looking pretty ugly right now. I don't want to have to redo the tables and keep migrating, though.

1/17/2023: Between me and Yvette we have a working preferences page, but the user is hard-coded and I can't find a good way to get the username of the logged in user. Riley told me I need to redo everything to work with Redux since the part Corey and Alex are working on already does. I don't want to have to scrap everything, but I might have to.

1/20/2023: Yvette and I did some unit tests and we settled on using checkboxes for the ingredients page to help filter the search results. I think everything is finally coming together.

1/21/2023: Met up on a Saturday, but it was worth it. We have filtered recipes! The checkboxes work and they send the query we want. This is the biggest part of the application now working. Next we can start cleaning up the code and adding the additional things.

1/24/2023: Today is finally the day I scrap all of my preferences page so I can redo it Redux. Haven't been looking forward to it, but I've at least come to understand Redux better now.

1/26/2023: Found an overlooked bug where logging out retains the query from the previous user. I was able to identify the cause, but I couldn't come up with a good solution. After working with Dalonte we agreed to just have the nav hard refresh the page when going to the Fridge, which resolves the issue, albeit not exactly how I would like. We're going to try deployment today, but if it doesn't work right away we'll probably put it off until after the exam.
