January 4th, 2023

- Started on our docker compose yaml file today and set up our databases. Looking at our wireframe honestly makes me nervous because it seems pretty complex but I'm glad my teammates are ambitious and highly motivated. So far we've got our fridge-api running successfully along with our postgres, ghi, and pg-admin containers. Also imposter syndrome is hitting harder than usual working in a group with such talent!

-

January 5th, 2023

- Today we finished up our migrations file and added the users table/model. This table will be referenced as a foreign key for the rest of our tables since most of the functionality is only accessible to account owners. Started on authentication today as well, but I'm still a little fuzzy so definitely have to watch the Curtis videos.

-

January 6th, 2023

- We're using Edamam API as our third party API to fetch recipes. We created a edamam queries and routers which are working properly and can return recipe labels when we search different ingredients on swagger. We decided that we want the name of the recipe, the url to the recipe, the image to be what gets returned for our search query.

-

January 9th, 2023

- Just to get things working we decided to combine all our tables in one migration file rather than separate files since we couldn't get migrations up that way. We figured its more important to get things up and running before anything else.

January 10th, 2023

- So far we've got our authentication working, and can successfully create an account, delete an account, list all existing accounts, and get an account by it's ID. For that to work the way it is now we had to scrap our users table, users queries, and router. We decided to join tables by an account id rather than a user id. Alex and Corey have made their fridge table, and started the fridge queries, and routers but nothing working yet. Jeremy and I have only made diet and health preferences tables. We ended up having to put all our tables under one migrations file just to simplify and make sure everything works properly before we separate our tables into different migrations files.

---

January 11th, 2023

- Jeremy and I revisited our preferences files and have full crud working with both diet and health preferences. Only issue is get health by ID is taking the health id, and not the account_id_health when trying to get a specific users preferences.

---

January 12th, 2023

- Jeremy the GOAT, and I created functions in the edamam queries to enhance our get_recipes_by_query function to also filter by our user preferences (diet and health). We now have two extra separate functions, get_diet, and get_health which takes diet and health preferences that are set to true and adds it to our request URL when a user searches for an ingredient or multiple ingredients. To make testing easier I also added an example schema to our basemodels for diet and health so we didn't have to go and manually set all the preferences to false. Instead now when we test in swagger it automatically has it defaulted to false.

-

January 13th, 2023

- Today Jeremy and I worked on unit tests for creating ingredients and get all ingredients. We followed Riley's demo and was able to get it working. I've started on testing token routes but only one test out of two passing so far.

-

January 18th, 2023

- Today Jeremy and I started on the preferences page on the front end. We started off just using useState hooks, then Jeremy added useEffect to get the drop down working. We got stuck and were advised to utilize redux toolkit so we may have to start from scratch with redux.

-

January 19th, 2023

- Finally fixed the token routes and have both unit tests passing! I was missing the authenticator which makes sense since users need to be authenticated when getting a token upon login.

-

January 20th, 2023

- Today I merged my working branch that had my token routes unit tests. Jeremy came across an issue where we would want a user, upon sign up to have a diet and health preferences default. After thinking about it we thought some users might not care to set a diet or health preference, and realized that the query doesn't work unless the user creates their diet and health preferences first. He got help from Riley and has it working how we want now. So when a user signs up their diet and health preferences default to false unless they want to update it and change certain items to true. Regardless the recipe search should work right after sign up!

-

January 21st, 2023

- It's the weekend before the project is due so on a Saturday we're going to get the main functionality finished. Alex and Corey figured out the checkboxes for our ingredients inventory so when a user checks an ingredient in their inventory then recipes that have that specific ingredient will show on the dashboard. Jeremy figured out that we still need to store the data (users checked ingredients) in a way that we can access it in our JSX with .map. We created another react hoook using useState to hold and set the users checked ingredients and then call that hook in our JSX. This was exciting to see happen! The main part of our app is basically complete! Even have the cards working too with pictures, label, and links to the actual recipe page.

-

January 23nd, 2023

- Now that the cards are working, we want to get them into a grid view so it's not one column of cards with a huge margin to the right of it! Jeremy figured out what class name we should be using, and combined that with the CSS style property I was using which was gridTemplateColumn and that seemed to work!

-

January 24th, 2023

- Now that the main dashboard functionality is working we're revisiting the user preferences as a group and got both the diet and health preferences to work. We initially wanted drop downs, but we figured for user friendliness we want the user to see what diet and health preferences they selected so we went with check boxes like we did with the dashboard/inventory.

-

January 25th, 2023

- We focused a little bit on bare minimum styling. Such as getting rid of the huge margins on the left and right side of the dashboard. I added a sticky header to the ingredients table so the column names didn't disappear when you scroll down. I also made the titles sticky so it didn't scroll with the contents. Then Corey came across a bug where you're able to sign up for a new account with a user that already exists. While I focused a little on the bare minimum styling the rest of the team were able to fix the bug.

-

January 26th, 2023

- Deployment sounds scary for us and most of the other groups. However my group gave it a shot even though I missed most of it. They met up with Dalonte and got close, but seems like when clicking the url to go to our webpage it's missing a lot of elements and we're not sure why. We have everything running how we want, looking how we want, and then Corey added another unit test to get all diets. We're basically at the finish line, just haven't deployed but we decided as a team to worry about that next week. Tomorrow is the due date for our project so we're all just looking forward to being done!
